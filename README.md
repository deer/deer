# deer

Here&rsquo;s a table of my public contact info:

| type                                                           | reference(s)                                                                                                                      |
| :------------------------------------------------------------- | :-------------------------------------------------------------------------------------------------------------------------------- |
| [email](https://en.wikipedia.org/wiki/Email)                   | `capreolinaⒶprotonmail◦ch`<sup><a href="#fn-1-1" id="fnm-1-1">\[1\]</a></sup>                                                                                        |
| [Discord&trade;](https://en.wikipedia.org/wiki/Discord)        | `capreolina`                                                                                                                       |
| [matrix](https://matrix.org/)                                  | `d33r:matrix.org`                                                                                                                 |
| [git](https://en.wikipedia.org/wiki/Git)                       | <https://codeberg.org/deer>, <https://codeberg.org/oddjobs>, <https://codeberg.org/Victoria>, <https://codeberg.org/subopt>       |
| git ([mirror](https://en.wikipedia.org/wiki/Mirror_site) only) | <https://notabug.org/deer>, <https://notabug.org/oddjobs>, <https://notabug.org/victoria-maple>, <https://notabug.org/suboptimal> |
| [<b>Oddjobs</b> forums](https://oddjobs.flarum.cloud/)         | `deer`                                                                                                                            |

Please note that **I do not use GitHub&trade;**; if you want to find my code, it&rsquo;s all over [at Codeberg](https://codeberg.org/deer) (see also: [the <b>Oddjobs</b> Codeberg profile](https://codeberg.org/oddjobs)). [The repositories that I have on NotABug](https://notabug.org/deer) are merely mirrors; [issues](https://en.wikipedia.org/wiki/Issue_tracking_system)/[<abbr title="pull requests">PRs</abbr>/<abbr title="merge requests">MRs</abbr>](https://stackoverflow.com/questions/23076923/what-is-a-merge-request) should be filed on Codeberg.

## my public key

(See: [OpenPGP](https://en.wikipedia.org/wiki/Pretty_Good_Privacy#OpenPGP).)

```text
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGBW2EABEADWebrBpwwobDQI4gqWWuiIPYqypBr71TpRRVFdhtpli4I0GLHx
moNmXDpeV+paZxxBDsjFFTckE7bnJq/F9Qv67rLL5n7dylx0sQBWlXFEaUJ3sNVX
j94jGx8PBZgiB2PGLlZ/4nlJFvYDU2MJUvsM+c4EVdgGt8z33ubkseI4DrP76sd0
Lr97PoC3RuvYd+z2vOAjtrS5uUHlf7DEWNpLgD41A9x1aIj5cx9XFiZeJFXmpjva
bdI6uF9JQqCGAn4sXpg2nSG8XxlhR2R8XRzWbzxTngCAMXruuH5V8QCNiWM0RZ2Z
9wDUdCdkbmBIPcSbsGKayLGCvaMqrotWXmp63eqXyP5lf8d4gXvggI8tSmliC6lR
oxVm3Zbx9Ihvd+5skdzHXiQOs+vmMpP3NZNAp2t5PeOLz8mmVHt8EFtxOQnKdb0e
u3WWV5YNSWyiL/hTTgu0pmEpfQozZAi33zeodiyFkB7ou+vm1nlGYbVLL/ac13tC
QFh0yr4omP1eEBRPAZOFBILI4+5A04wfs5DMthSpm5AF/Oi7yXYjcuJtM/HkqzBw
teBSglL7O0KWlgznBfFDjqd7r5w5pqEDl6boh7WjzoF2TvggZ12eMPAugmeHG46n
aYZd4J8Lk4/gYpOwDfPHGyNtvfZkDScP0O4SAjyVSK7rIFfcGf9Cl/mztwARAQAB
tCRkZWVyIGRlZXIgPGNhcHJlb2xpbmFAcHJvdG9ubWFpbC5jaD6JAk4EEwEKADgW
IQTpTH+pbTG7ktZ+GlEjX4uqvc5wXgUCYFbYQAIbAwULCQgHAgYVCgkICwIEFgID
AQIeAQIXgAAKCRAjX4uqvc5wXimHD/9k1VsLg97P48QWtkVSUaXQrc+nBKRXxtZ1
kOWQqqX8BqpTTxIQAHqOKmAt+TmoZPqYj499jUl0ltks8PIPIwnV7yzhFrorxsR9
+FKYA4A02t9QgLqQsQe9wssu+fsFJ/zfq0Rbiyv5fdpAEJznUE9mWN5cavoxPp7n
TecKokB+dJ2FDlugBxEU981XFsOlmD1ZLjJCbl/afNJs7KVFtsgnutI8KCehl0ey
nkf0x+RWBv4t6tnWxJvCxIkrTt4uFYd2SoR4UdWo8zCyBZk6g5AsVbivtraERJcI
z3bXUz/stY6oMLOoU26i+HJuEhJwGTpOwt223z6qAIKeJT4Q53xn9UWr72NsUicD
m9R/CsgBvgBjP3qOOvP0Lt7tDUlTFL4JmmRnJ4bfjdYBkz0BAWFgANQC2H/AO8iM
UTZAhcpQ1ikQx/XYnmX7lpioISuymLSQ2Wi3m9G7asKyWgolQTlHqxEFgctDgK7G
fqsKXHYPexKiCXQ+C7UdTmInYyByAWRcs3PjmzWQC7YwAhBMVgC+vkKw9K68pM2M
8EfkipQH2OmmHmnZwMJeaustI/SyR44n/os14zKEzkv21R20ESjM+OdqfKzioOu6
FxCjFCscqwq1N6s1h7MZ0xdgepU3rhYnPjG2ksWqTUnyZaVcNA5kYMigiVN3CyLM
lkWjD8PKfLkCDQRgVthAARAA1MCvJlf2iwY+eToU24etUpufTzIeTL084JbNT1r/
64lf/EDuMZx/tc9An704WL41bG9HBIkaNyIxf0zg1AsV7wR6CWVyk9K0bBGrCQXh
Lkev9GGqFFnfdVgSBfg+AmU5ymzoqmoSVO8BV0c+xM/TPJKxSEghuVMCaCRhZbLB
vagyWVdEjgDzuN8bdZXeH9cpbmLhDo1X92BhYA6mTkKwV/wAGxpmR+yZfPL7EB+U
Q/nB3ay0qKSxTMEJHapy9oNcfK7mBf0ujimEqZ4KRcp8D1wu95XuwPsP9yA3VCDr
HFfOu32jBCNBWsnAqJeK5H3x4klacTupg9HzBmIPdtw99gfYmOPBZkPsFQ+A1zEF
9/kFnvJ4MKCYSG5c+5zVkqc6t6lpKvosZKMjsMm64LwsZ91ugYEVoHeAfZs+Myso
qTgnbUeHx9cE78ZL7nnFBzdxA+MxzNXaDktHuZfalAtN2aTbjHeY3y/qLEaFbJq4
WTYsXZPHGMXjwp0M0wF0yRYYLHNWQ3Ke7tK4pklVasMmnwqNLJOqIz39mivfp5+M
e1XEuWpdqupJ+Izj7sqidx4MVD/b4ufcaSo+Un7gn9NB73wNymQl6ldyBWnGp38Z
avyNVhAQ/g+Rm0Ysazr7CuIuVXWgJWjPMUeW8ZxMYxldjlvmPz0B+oPu6jjyjT+M
2usAEQEAAYkCNgQYAQoAIBYhBOlMf6ltMbuS1n4aUSNfi6q9znBeBQJgVthAAhsM
AAoJECNfi6q9znBeA2cP/0utqHTHP4LWJnu2uMtoW4lDZ55zbjQE0K6ZLWaz8Fkj
oa/mikbXyuofMKgkhFXzv6pe+V9aGDtJnopeqFuS9KWwLm4c2s4PA+ELRM0lC4rc
mR5JVU5xrmIaNdS5MdJm4CroAl9XJNaTlg4xiUiGNOugXwRBnVdwE/kZe3NWVcQa
bs8FTJ6LbP/I62a3ZrHLdHsR+V3SBWZuBBEK0gdNrQmnYHUazcLkIV5E8Gg3Xc6+
XL37yi/I3IJJ1fsCTycY4Ee5Y+xouZREJ/hNgqZII8ZzD2uN3Fg00OIRX/KI/+9v
vEa5tfFaTO0V0E3H343kC45mV3S1kTthM7VwcfTEQjzsFBcFgII1FtuzVetCw+aO
ewUUEl0+W0qAb9p3dehEWCvMiT1slYneB/7FJ/4tPZdNleDLS7qeIHJBHRqiND0d
4gnCagynEq+jq+Hhkeu/ns0dbjT0GGQZWSwj4JgYJvr9SoBX+YVGEbt5nGgASqr3
L05GcAXhxu/0M06TXg430kWnIIvAnHpgPARaIx64oga5mc7/0GCsjFjIavqTGKtl
UOOsLdX12uxMVQW4cY7PhdAYj9TUUHjfanls3gc9UaEHGNt0ZDddzbKuEW0JLmGz
hCSqbcIzf6lBRfyoO+zky28ld0NWKEuKQB2FtK2M4f1lgytaDCacO++jwZB9G/B0
=rDUt
-----END PGP PUBLIC KEY BLOCK-----
```

---

## Footnotes for “how to contact me”

1. <a href="#fnm-1-1" id="fn-1-1">\[↑\]</a> Replace the `Ⓐ` with an `@`, and the `◦` with a `.`.
